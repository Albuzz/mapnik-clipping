Original Layer Spatial Reference System for tif file (according to QGIS):
`+proj=merc +lon_0=15.05830001831055 +lat_ts=59.37874984741211 +x_0=0 +y_0=0 +a=6378137 +b=6378137 +units=m +no_defs`

Bounding boxes computed in QGIS, based on [this shapefile of municipality borders](http://www.arcgis.com/home/item.html?id=41f5d23fef8f410590f2d934c7dba81a).