import mapnik
import csv

# Skip this for now
# from_srs = mapnik.Projection('+proj=merc +lon_0=15.05830001831055 '
#                              '+lat_ts=59.37874984741211 +x_0=0 +y_0=0 '
#                              '+a=6378137 +b=6378137 +units=m +no_defs')
# to_srs = mapnik.Projection('+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 '
#                            '+lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m '
#                            '+nadgrids=@null +no_defs +over')

imgx = 600
imgy = 600
stylesheet = 'style.xml'

m = mapnik.Map(imgx, imgy)
mapnik.load_map(m, stylesheet)
m.zoom_all()
row_name = 'test'
mapnik.render_to_file(m, 'test.png')
# Skip this for now
# transform = mapnik.ProjTransform(from_srs, to_srs)

# with open('bboxes_orebro.csv', 'r') as csvfile:
#     reader = csv.reader(csvfile)
#     headers = reader.next()
#     for row in reader:
#         row_id = row[4]
#         row_name = row[5]
#         bounds = map(float, row[0:4])
#         bbox = mapnik.Box2d(*bounds)
#         # merc_box = transform.forward(bbox)
#         m.zoom_to_box(bbox)
#         mapnik.render_to_file(m, 'rendered_images/%s.png' % row_name)
